<?php
    public function checkAuthenticHost() {
        $CURRENT_ENVIRONMENT = $_SESSION['env'];
        $SERVER_NAME = $_SERVER['SERVER_NAME'];

        // echo $CURRENT_ENVIRONMENT;
        // echo $SERVER_NAME;

        $JSOM_PROD_ENVIRONMENT = "prod";
        $JSOM_PROD_HOST = "jsom.utdallas.edu";

        $JSOM_DEV_ENVIRONMENT = "dev";
        # $JSOM_DEV_HOST = "is-submit.lndo.site";
        $JSOM_DEV_HOSTS = ["websitesdev.utdallas.edu","lndo.site","localhost","127.0.0.1"];

        if (($CURRENT_ENVIRONMENT === $JSOM_PROD_ENVIRONMENT and $SERVER_NAME  === $JSOM_PROD_HOST) or ($CURRENT_ENVIRONMENT === $JSOM_DEV_ENVIRONMENT and in_array($SERVER_NAME, $JSOM_DEV_HOSTS))) {
        // echo "Looks good, Authentic Host - AK Troubleshooting";
        return true;
        }

        // echo "Bad Host!";
        return false;
    }
?>